import { createApp } from 'vue';
import App from './App.vue';
import router from './router/route.index';
import store from './store/store.index.js';
import './styles/tailwind.css';
import 'element-plus/theme-chalk/src/message.scss';
import 'element-plus/theme-chalk/src/message-box.scss';
import 'element-plus/theme-chalk/src/loading.scss';

createApp(App).use(router).use(store).mount('#app');
