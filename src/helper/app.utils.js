import { axiosAuth } from './axios.config';
import store from '../store/store.index';
import { computed } from 'vue';
import { tokenCookie } from './utils';
import jwt_decode from 'jwt-decode';

export const initialLogInUser = (token, leftMenu, adminStatus) => {
  // insert token to global state, axios & set authenticated
  // insert leftMenu to global state
  if (token && leftMenu) {
    const payload = {
      token: token,
      adminStatus: adminStatus,
    };
    store.commit('setAuth', payload);
    store.commit('setLeftMenu', leftMenu);
    axiosAuth.defaults.headers.common['Authorization'] = `Bearer ${token}`;
  }
  return store.getters.isAuth;
};

export function labelPosition() {
  const isOnMobile = computed(() => store.getters.isOnMobile);
  if (isOnMobile.value) return 'top';
  return 'left';
}

export function checkToken() {
  //return true if there is a token & leftMenu
  //register token if state has no token & leftMenu

  let token = store.getters.token;
  const isAuth = store.getters.isAuth;
  let leftMenu = store.getters.leftMenu;

  if (token && isAuth && leftMenu) return true;

  token = tokenCookie();
  leftMenu = JSON.parse(localStorage.getItem('leftMenu'));
  if (token && leftMenu) {
    const { ADMIN } = jwt_decode(token);
    const isAuth = initialLogInUser(token, leftMenu, ADMIN);
    if (isAuth) {
      const decoded = jwt_decode(token);
      saveUser(decoded);
    }
    return isAuth;
  }
  return false;
}

export function saveUser(data) {
  if (data) {
    const user = {
      id: data.ADM_USER_ID,
      name: data.ADM_USER_NAME,
      role: data.ROLE_NAME,
    };
    store.commit('setUser', user);
  }
}
