import { ElLoading } from 'element-plus';
import { ElMessage } from 'element-plus';
import Cookies from 'js-cookie';
import jwt_decode from 'jwt-decode';
import moment from 'moment';

export const dillScssExportToJson = (scssExportJson) => {
  let jsonString = scssExportJson.replace(/:export\s*/, '').replace(/[\s+\r\n]/g, '');
  let scssJson = {};
  jsonString
    .slice(1, jsonString.length - 2)
    .split(';')
    .forEach((fItem) => {
      let arr = fItem.split(':');
      scssJson[arr[0]] = arr[1];
    });
  return scssJson;
};

export const isFormValid = (elForm) => {
  return new Promise((resolve) => {
    elForm.validate((isValid) => resolve(isValid));
  });
};

export const loading = () => {
  const load = ElLoading.service({
    lock: true,
    text: 'Loading',
    background: 'rgba(0, 0, 0, 0.7)',
  });
  return load;
};

export const messageInfo = (message, type) => {
  return ElMessage({
    message: message,
    type: type ? type : 'error',
  });
};

export const saveTokenToCookie = async (jwtToken) => {
  if (!jwtToken) throw new TypeError('Token is undefined - utils.js 42');
  const decoded = await jwt_decode(jwtToken);
  const asDate = new Date(decoded.exp * 1e3);
  //cookies will be automatically deleted by the browser if it expires
  Cookies.set('ACCESS', jwtToken, { expires: asDate, secure: false });
};

export const getDifferentObject = (object1, object2) => {
  let returnObj = {};
  Object.entries(object1).forEach((element) => {
    const [key, value] = element;
    if (object1[key] != object2[key]) {
      returnObj[key] = value;
    }
  });
  return returnObj;
};

export const valueOrUndefined = (value) => {
  if (value) return value;
  return undefined;
};

export const paginateObj = (pageIndex, pageSize, textSearch) => {
  return {
    PAGE_INDEX: pageIndex,
    DATA_PER_PAGE: pageSize,
    TEXT_SEARCH: valueOrUndefined(textSearch),
  };
};

export const paginateObjNew = (pageIndex, pageSize, textSearch, newObj = {}) => {
  var obj = {
    PAGE_INDEX: pageIndex,
    DATA_PER_PAGE: pageSize,
    TEXT_SEARCH: valueOrUndefined(textSearch),
  };
  obj = { ...obj, ...newObj };

  return obj;
};

export const paginateObjOpt = (pageIndex, pageSize, options) => {
  const page = {
    PAGE_INDEX: pageIndex,
    DATA_PER_PAGE: pageSize,
    ...options,
  };
  return page;
};
export const detailObj = (options) => {
  const page = {
    ...options,
  };
  return page;
};

export const isObjectEmpty = (object) => {
  if (!object) return true;
  if (Object.keys(object).length === 0) return true;
  return false;
};

export const convertRp = (money) => {
  if (!money) return 'Rp 0,00';
  return new Intl.NumberFormat('id-ID', { style: 'currency', currency: 'IDR', maximumFractionDigits: 0 }).format(parseInt(money));
};

export const convertMoneyCompact = (money) => {
  if (!money) return 'Rp 0,00';
  return new Intl.NumberFormat('id-ID', { notation: 'compact' }).format(parseInt(money));
};

export function tokenCookie() {
  return Cookies.get('ACCESS');
}

export function dateTime(stringDate) {
  // if (stringDate) return moment(stringDate).format('MMMM DD YYYY, h:mm');
  if (stringDate) {
    let date;
    if (stringDate.substring(stringDate.length - 5) === '.000Z') {
      date = stringDate.slice(0, -5);
    } else {
      date = stringDate;
    }
    return moment(date).utc().format('MMMM DD YYYY, h:mm a');
  }
}
export function dateTime2(stringDate) {
  // if (stringDate) return moment(stringDate).format('MMMM DD YYYY, h:mm');
  if (stringDate) {
    let date;
    if (stringDate.substring(stringDate.length - 5) === '.000Z') {
      date = stringDate.slice(0, -5);
    } else {
      date = stringDate;
    }
    return moment(date).utc().format('MMMM DD YYYY, h:mm');
  }
}
export function dateTimeAM(stringDate) {
  // if (stringDate) return moment(stringDate).format('MMMM DD YYYY, h:mm');
  if (stringDate) {
    let date;
    if (stringDate.substring(stringDate.length - 5) === '.000Z') {
      date = stringDate.slice(0, -5);
    } else {
      date = stringDate;
    }
    return moment(date).utc().format('MMMM DD YYYY, h:mm a');
  }
}
export function time(stringDate) {
  if (stringDate) {
    let date;
    if (stringDate.substring(stringDate.length - 5) === '.000Z') {
      date = stringDate.slice(0, -5);
    } else {
      date = stringDate;
    }
    return moment(date).utc().format('h:mm a');
  }
}
export function time2(stringDate) {
  if (stringDate) {
    let date;
    if (stringDate.substring(stringDate.length - 5) === '.000Z') {
      date = stringDate.slice(0, -5);
    } else {
      date = stringDate;
    }
    return moment(date).utc().format('h:mm');
  }
}
export function time24(stringDate) {
  if (stringDate) {
    let date;
    if (stringDate.substring(stringDate.length - 5) === '.000Z') {
      date = stringDate.slice(0, -5);
    } else {
      date = stringDate;
    }
    return moment(date).utc().format('HH:mm');
  }
}
export function date(stringDate) {
  // if (stringDate) return moment(stringDate).format('MMMM DD YYYY, h:mm');
  if (stringDate) {
    let date;
    if (stringDate.substring(stringDate.length - 5) === '.000Z') {
      date = stringDate.slice(0, -5);
    } else {
      date = stringDate;
    }
    return moment(date).utc().format('MMMM DD YYYY');
  }
}

export function dateDOB(stringDate) {
  if (stringDate) {
    let date;
    if (stringDate.substring(stringDate.length - 5) === '.000Z') {
      date = stringDate.slice(0, -5);
    } else {
      date = stringDate;
    }
    return moment(date).utc().format('YYYY-MM-DD');
  }
}

export function epochData(epoch) {
  return moment.unix(epoch).utc().format('MMMM DD YYYY, h:mm a');
}
export function epochDataAM(epoch) {
  return moment.unix(epoch).utc().format('MMMM D, YYYY h:mm A');
}
export function formatDate(date) {
  var d = new Date(date),
    month = '' + (d.getMonth() + 1),
    day = '' + d.getDate(),
    year = d.getFullYear();

  if (month.length < 2) month = '0' + month;
  if (day.length < 2) day = '0' + day;

  return [year, month, day].join('-');
}

export function decodePLusString(data) {
  try {
    return decodeURI(data).replace(/(<([^>]+)>)/gi, '');
  } catch (e) {
    return data.replace(/(<([^>]+)>)/gi, '');
  }
}

export function decodeFunction(data) {
  try {
    return decodeURI(data);
  } catch (e) {
    return data;
  }
}
export function phoneNo(data) {
  // var cleaned = ('' + data).replace(/\D/g, '');
  var match = data.match(/^(\d{2})(\d{4})(\d{4})(\d{3})$/);
  if (match) {
    return '(' + '+' + match[1] + ') ' + match[2] + '-' + match[3] + '-' +match[4];
  }
  return data;
}
