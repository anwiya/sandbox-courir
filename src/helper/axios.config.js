import axios from 'axios';
import envConfig from './env.config';

const url = envConfig.isProd ? envConfig.apiProd : envConfig.apiDev;
export const axiosNotAuth = axios.create({
  baseURL: url,
  timeout: 10000,
});
axiosNotAuth.defaults.headers.common['Content-Type'] = 'application/json';

export const axiosAuth = axios.create({
  baseURL: url,
  timeout: 10000,
  //   withCredentials: true,
});
axiosAuth.defaults.headers.common['Content-Type'] = 'application/json';

export default { axiosNotAuth, axiosAuth };
