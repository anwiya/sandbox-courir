const env = {
  mode: import.meta.env.MODE,
  baseUrl: import.meta.env.BASE_URL,
  apiProd: import.meta.env.VITE_API_URL_PROD,
  apiDev: import.meta.env.VITE_API_URL_DEV,
  appVersion: import.meta.env.VITE_APP_VERSION,
  prodName: 'production',
  devName: 'development',
  apiPrefix: import.meta.env.VITE_API_PREFIX,
  isProd: import.meta.env.PROD,
  isDev: import.meta.env.DEV,
  pageSize: import.meta.env.VITE_PAGE_SIZE,
};

export default {
  ...env,
};
