export const requiredValMsg = () => `Required field`;

export const maxValMsg = (totalChar) => {
  return `Length should not exceed ${totalChar} characters`;
};

export const requiredRule = (message = requiredValMsg(), required = true, trigger = ['change']) => {
  return {
    message: message,
    required: required,
    trigger: trigger,
  };
};

export const maxStringRule = (max, trigger = ['blur', 'change'], message) => {
  return {
    max: max,
    trigger: trigger,
    message: message ? message : maxValMsg(max),
  };
};

export const emailRule = (message = 'Please input correct email address', trigger = ['blur', 'change'], type = 'email') => {
  return {
    message: message,
    trigger: trigger,
    type: type,
  };
};

export const minNumberRule = (inputNumber = 1, trigger = ['blur', 'change']) => {
  return {
    validator: (_, value) => value >= inputNumber,
    message: `Must be greater than or equal to ${inputNumber}`,
    trigger: trigger,
  };
};

export const maxNumberRule = (inputNumber = 2147483647, trigger = ['blur', 'change']) => {
  return {
    validator: (_, value) => value < inputNumber,
    message: `Must be smaller than or equal to ${inputNumber}`,
    trigger: trigger,
  };
};
