import { messageInfo, loading } from './utils';

export function catchError(functionExec, errorCallback, finallyCallback, options = { load: true }) {
  let load;
  if (options.load) {
    load = loading();
  }

  functionExec()
    .catch((error) => {
      const message = error.response?.data?.message ? error.response.data.message : null;
      const errorName = error.name;
      const isAxiosError = errorName == 'AxiosError' ? true : false;
      const errorCode = error.code;
      const errorMessage = error.message;
      if (message) {
        messageInfo(`${error.response.status} : ${message}`);
      } else if (isAxiosError && errorCode == 'ERR_NETWORK') {
        messageInfo('No connection, check your internet');
      } else if (isAxiosError) {
        messageInfo(`${errorCode} : ${errorMessage}`);
      } else {
        messageInfo(error);
      }
      if (errorCallback) {
        errorCallback();
      }
    })
    .finally(() => {
      if (options.load) {
        load.close();
      }
      if (finallyCallback) {
        finallyCallback();
      }
    });
}
