import * as api from '../../api/dashboard.api';
import { ref } from 'vue';
import { catchError } from '../../helper/async.catch';
import { messageInfo } from '../../helper/utils';
import viewOne from '../../hooks/view-one';
import { formatDate } from '../../helper/utils';
import useGet from '../../hooks/get';

export function createConsult() {
  const [statusWa, getWaStatus] = useGet();
  const [statusEmail, getEmailStatus] = useGet();
  const arrData = ['TOTAL_TC', 'TOTAL_REQUEST_TC', 'TOTAL_ONGOING_TC', 'TOTAL_FINISH_TC', 'TOTAL_FINISH_TC_CONSUL', 'TOTAL_FINISH_TC_RECIPE'];
  const date = new Date();
  const year = date.getFullYear();
  const month = date.getMonth() + 1;
  const tgl = formatDate(`${year}-${month}-01`);
  const dateDate = formatDate(date);
  const from = ref(tgl);
  const to = ref(dateDate);
  const tipe = ref([]);
  const consult = ref({});

  async function getConsult() {
    if (!from.value || !to.value) {
      return messageInfo('Please select date');
    }
    catchError(async () => {
      const { data } = await api.consult(from.value, to.value);
      consult.value = data?.data;
    });
  }
  async function getView(index) {
    catchError(async () => {
      const { data } = await api.tipe(from.value, to.value, arrData[index]);
      tipe.value = data?.data;
    });
  }

  const { viewData, viewDialog, openViewDialog } = viewOne();

  function onViewDialog(currentRowObj) {
    getView(currentRowObj);
    openViewDialog(currentRowObj);
  }

  // Get Status
  getWaStatus(api.waStatus);
  getEmailStatus(api.emailStatus);

  return [viewData, viewDialog, from, to, consult, tipe, getConsult, onViewDialog, statusWa, statusEmail];
}

// export function createView() {
//   const arrData = ['TOTAL', 'REQUEST', 'ONGOING', 'FINISH', 'FINISH_CONSUL', 'FINISH_RECEIPE'];
//   const date = new Date();
//   const year = date.getFullYear();
//   const month = date.getMonth() + 1;
//   const tgl = ref(`${year}-0${month}-01`);
//   const dateDate = moment(date).format('YYYY-MM-DD');
//   const from = ref(tgl);
//   const to = ref(dateDate);
//   const type = ref(arrData);
//   const tipe = ref([]);

//   async function getView(index) {
//     catchError(async () => {
//       const { data } = await api.tipe(from.value, to.value, arrData[index]);
//       tipe.value = data?.data;
//     });
//   }

//   const { viewData, viewDialog, openViewDialog } = viewOne();

//   function onViewDialog(currentRowObj) {
//     getView(currentRowObj);
//     openViewDialog(currentRowObj);
//   }
//   return [viewData, viewDialog, tipe, onViewDialog, type];
// }

export function createDoctor() {
  const date = new Date();
  const year = date.getFullYear();
  const month = date.getMonth() + 1;
  const tgl = formatDate(`${year}-${month}-01`);
  const dateDate = formatDate(date);
  const from = ref(tgl);
  const to = ref(dateDate);
  const loadData = ref(true);
  const lapScreen = ref(true);
  const numScreen = ref(3);
  const doctorNotNull = ref(true);
  const doctors = ref([]);

  function getScreen() {
    let width = window.screen.width;
    if (width > 1366) {
      lapScreen.value = false;
      numScreen.value = 4;
    } else if (width <= 768) {
      numScreen.value = 2;
    } else {
      lapScreen.value = true;
    }
  }

  async function getDoctors() {
    loadData.value = false;
    getScreen();
    if (!from.value || !to.value) {
      return messageInfo('Please select date');
    }
    catchError(async () => {
      const { data } = await api.doctors(from.value, to.value);
      doctors.value = data?.data;
    });
  }

  function hideDoctor() {
    getScreen();
    loadData.value = true;
    if (!from.value || !to.value) {
      return messageInfo('Please select date');
    }
    catchError(async () => {
      const { data } = await api.doctors(from.value, to.value);
      const doctorData = data?.data;

      if (doctorData == undefined) {
        doctors.value = [];
        doctorNotNull.value = false;
      } else {
        doctors.value = [];
        for (let i = 0; i < doctorData.length; i++) {
          if (i == numScreen.value) {
            break;
          }
          doctors.value.push(doctorData[i]);
        }
      }
    });
  }

  return [from, to, doctors, getDoctors, hideDoctor, loadData, lapScreen, doctorNotNull];
}
