import { ref } from 'vue';
import * as util from '../../helper/utils';
import { catchError } from '../../helper/async.catch';
import * as api from '../../api/auth.api';
import * as appUtil from '../../helper/app.utils';

export function createLoginForm(router) {
  const loginData = ref({});
  const loginForm = ref(null);

  async function onSubmit() {
    const resultVal = await util.isFormValid(loginForm.value);
    if (!resultVal) return;
    catchError(async () => {
      const { data } = await api.login(loginData.value);
      const token = data.data?.token;
      const leftMenu = data.data?.leftMenus;
      const adminStatus = data.data?.ADMIN;
      if (!token) throw new Error('Failed to get token');
      const isAuth = appUtil.initialLogInUser(token, leftMenu, adminStatus);
      if (isAuth) {
        appUtil.saveUser(data?.data);
        await util.saveTokenToCookie(token);
        localStorage.setItem('leftMenu', JSON.stringify(leftMenu));
        util.messageInfo('Success Log in', 'success');
        return router.replace('/dashboard');
      }
    });
  }

  return [loginData, loginForm, onSubmit];
}
