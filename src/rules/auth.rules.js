import * as rl from '../helper/rules';

export const login = {
  ADM_USER_EMAIL: [rl.requiredRule(), rl.emailRule()],
  ADM_USER_PASSWORD: [rl.requiredRule()],
};