import { createStore } from 'vuex';

export default createStore({
  state: {
    isCollapse: false,
    token: 'anjhfowehifhoikdhkoajshd',
    isAuth: true,
    isLoad: false,
    isOnMobile: false,
    currentMenuName: '',
    leftMenu: [
      {
        MENU_HEADER_CODE: 'DASH',
        MENU_HEADER_NAME: 'Dashboard',
        MENU_HEADER_PICTURE:
          '%3Csvg%20xmlns=%22http://www.w3.org/2000/svg%22%20width=%2216%22%20height=%2216%22%20fill=%22currentColor%22%20class=%22bi%20bi-graph-up-arrow%22%20viewBox=%220%200%2016%2016%22%3E%3Cpath%20fill-rule=%22evenodd%22%20d=%22M0%200h1v15h15v1H0V0Zm10%203.5a.5.5%200%200%201%20.5-.5h4a.5.5%200%200%201%20.5.5v4a.5.5%200%200%201-1%200V4.9l-3.613%204.417a.5.5%200%200%201-.74.037L7.06%206.767l-3.656%205.027a.5.5%200%200%201-.808-.588l4-5.5a.5.5%200%200%201%20.758-.06l2.609%202.61L13.445%204H10.5a.5.5%200%200%201-.5-.5Z%22/%3E%3C/svg%3E',
        MENU_HEADER_PATH: 'dashboard',
        MENU_HEADER_HAS_CHILDREN: false,
        childrenMenu: [],
      },
      {
        MENU_HEADER_CODE: 'QUOT',
        MENU_HEADER_NAME: 'Cari Penawaran',
        MENU_HEADER_PICTURE:
          '%3Csvg%20xmlns=%22http://www.w3.org/2000/svg%22%20width=%2216%22%20height=%2216%22%20fill=%22currentColor%22%20class=%22bi%20bi-postcard%22%20viewBox=%220%200%2016%2016%22%3E%3Cpath%20fill-rule=%22evenodd%22%20d=%22M2%202a2%202%200%200%200-2%202v8a2%202%200%200%200%202%202h12a2%202%200%200%200%202-2V4a2%202%200%200%200-2-2H2ZM1%204a1%201%200%200%201%201-1h12a1%201%200%200%201%201%201v8a1%201%200%200%201-1%201H2a1%201%200%200%201-1-1V4Zm7.5.5a.5.5%200%200%200-1%200v7a.5.5%200%200%200%201%200v-7ZM2%205.5a.5.5%200%200%201%20.5-.5H6a.5.5%200%200%201%200%201H2.5a.5.5%200%200%201-.5-.5Zm0%202a.5.5%200%200%201%20.5-.5H6a.5.5%200%200%201%200%201H2.5a.5.5%200%200%201-.5-.5Zm0%202a.5.5%200%200%201%20.5-.5H6a.5.5%200%200%201%200%201H2.5a.5.5%200%200%201-.5-.5ZM10.5%205a.5.5%200%200%200-.5.5v3a.5.5%200%200%200%20.5.5h3a.5.5%200%200%200%20.5-.5v-3a.5.5%200%200%200-.5-.5h-3ZM13%208h-2V6h2v2Z%22/%3E%3C/svg%3E',
        MENU_HEADER_PATH: 'quotation',
        MENU_HEADER_HAS_CHILDREN: false,
        childrenMenu: [],
      },
      {
        MENU_HEADER_CODE: 'ORDR',
        MENU_HEADER_NAME: 'Buat Order',
        MENU_HEADER_PICTURE:
          '%3Csvg%20xmlns=%22http://www.w3.org/2000/svg%22%20width=%2216%22%20height=%2216%22%20fill=%22currentColor%22%20class=%22bi%20bi-postcard%22%20viewBox=%220%200%2016%2016%22%3E%3Cpath%20fill-rule=%22evenodd%22%20d=%22M2%202a2%202%200%200%200-2%202v8a2%202%200%200%200%202%202h12a2%202%200%200%200%202-2V4a2%202%200%200%200-2-2H2ZM1%204a1%201%200%200%201%201-1h12a1%201%200%200%201%201%201v8a1%201%200%200%201-1%201H2a1%201%200%200%201-1-1V4Zm7.5.5a.5.5%200%200%200-1%200v7a.5.5%200%200%200%201%200v-7ZM2%205.5a.5.5%200%200%201%20.5-.5H6a.5.5%200%200%201%200%201H2.5a.5.5%200%200%201-.5-.5Zm0%202a.5.5%200%200%201%20.5-.5H6a.5.5%200%200%201%200%201H2.5a.5.5%200%200%201-.5-.5Zm0%202a.5.5%200%200%201%20.5-.5H6a.5.5%200%200%201%200%201H2.5a.5.5%200%200%201-.5-.5ZM10.5%205a.5.5%200%200%200-.5.5v3a.5.5%200%200%200%20.5.5h3a.5.5%200%200%200%20.5-.5v-3a.5.5%200%200%200-.5-.5h-3ZM13%208h-2V6h2v2Z%22/%3E%3C/svg%3E',
        MENU_HEADER_PATH: 'order',
        MENU_HEADER_HAS_CHILDREN: false,
        childrenMenu: [],
      },
    ],

    isAdmin: true,
    user: {
      id: '123',
      name: 'samtech',
      role: 'admin',
    },
  },
  mutations: {
    setCollapse(state) {
      state.isCollapse = !state.isCollapse;
    },
    setAuth(state, payload) {
      state.isAuth = true;
      state.token = payload.token;
      state.isAdmin = payload.adminStatus;
    },
    setLoad(state) {
      state.isLoad = true;
    },
    unsetLoad(state) {
      state.isLoad = false;
    },
    setOnMobile(state) {
      state.isOnMobile = true;
      state.isCollapse = true;
    },
    setLogout(state) {
      state.token = null;
      state.isAuth = false;
      state.isAdmin = false;
    },
    setCurrentMenuName(state, name) {
      state.currentMenuName = name;
    },
    setLeftMenu(state, leftMenu) {
      state.leftMenu = leftMenu;
    },
    setUser(state, user) {
      state.user.id = user.id;
      state.user.name = user.name;
      state.user.role = user.role;
    },
  },
  getters: {
    isCollapse(state) {
      return state.isCollapse;
    },
    token(state) {
      return state.token;
    },
    isAuth(state) {
      return state.isAuth;
    },
    isOnMobile(state) {
      return state.isOnMobile;
    },
    currentMenuName(state) {
      return state.currentMenuName;
    },
    leftMenu(state) {
      return state.leftMenu;
    },
    isAdmin(state) {
      return state.isAdmin;
    },
    user(state) {
      return state.user;
    },
  },
  actions: {},
});
