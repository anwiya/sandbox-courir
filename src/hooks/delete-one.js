import { messageInfo } from '../helper/utils';
import { catchError } from '../helper/async.catch';

export default function useDeleteOne(options = { useArray: false }) {
  async function deleteData(callBack, apiURL, OBJECT_ID) {
    catchError(async () => {
      const { status } = await apiURL(OBJECT_ID);
      if (status == 204) {
        messageInfo('Successfully delete data', 'success');
        callBack();
      }
    });
  }

  if (options.useArray) {
    return [deleteData];
  } else {
    return { deleteData };
  }
}
