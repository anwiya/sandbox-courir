import { ref } from 'vue';
import { isFormValid, messageInfo, getDifferentObject } from '../helper/utils';
import { catchError } from '../helper/async.catch';

export default function useEditoptions(options = { useArray: false }) {
  const editData = ref({});
  const editForm = ref(null);
  const editDialog = ref(false);
  let editOrigin = {};

  const openEditDialog = async (objectEdit) => {
    editData.value = { ...objectEdit }; //create new object v1
    editDialog.value = true;
    editOrigin = JSON.parse(JSON.stringify(objectEdit)); //create new object v2
  };

  const saveEdit = async (callback, apiURL, options = {}) => {
    const resultVal = await isFormValid(editForm.value);
    if (!resultVal) return;
    catchError(async () => {
      const { status, data } = await apiURL(options);
      if (status == 200) {
        messageInfo('Successfully update data', 'success');
        editDialog.value = false;
        callback(data.data);
      }
    });
  };

  const cancelEdit = () => {
    editDialog.value = false;
    editForm.value.clearValidate();
  };

  if (options.useArray) {
    return [editData, editForm, editDialog, openEditDialog, saveEdit, cancelEdit];
  } else {
    return { editData, editForm, editDialog, openEditDialog, saveEdit, cancelEdit };
  }
}
