import { ref, watch } from 'vue';
import { isFormValid, messageInfo } from '../helper/utils';
import { catchError } from '../helper/async.catch';

export default function useAddOne(options = { useArray: false }) {
  const addData = ref({});
  const addForm = ref(null);
  const addDialog = ref(false);

  watch(addDialog, () => {
    if (!addDialog.value && addForm.value) {
      addForm.value.clearValidate();
      addForm.value.resetFields();
    } else if (addForm.value) {
      addForm.value.clearValidate();
      addForm.value.resetFields();
      addForm.value = null;
    }
  });

  watch(addForm, () => {
    if (addForm.value && addDialog.value) {
      addForm.value.$el[0].focus();
    }
  });

  const saveAdd = async (callback, apiURL) => {
    const resultVal = await isFormValid(addForm.value);
    if (!resultVal) return;
    catchError(async () => {
      const { status, data } = await apiURL(addData.value);
      if (status == 201 || status == 200) {
        addDialog.value = false;
        messageInfo('Successfully save data', 'success');
        callback(data.data);
      }
    });
  };

  const cancelAdd = () => {
    addDialog.value = false;
  };

  if (options.useArray) {
    return [addData, addForm, addDialog, saveAdd, cancelAdd];
  } else {
    return { addData, addForm, addDialog, saveAdd, cancelAdd };
  }
}
