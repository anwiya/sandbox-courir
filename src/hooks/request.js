import { catchError } from '../helper/async.catch';
import { upload } from '../api/upload.api';
import reduce from 'image-blob-reduce';

export function requestUpload(file, onError) {
  return new Promise((resolve) => {
    // execution will stop when an error occurs
    catchError(
      async () => {
        const blob = await reduce().toBlob(file, { max: 1000 });
        const { data } = await upload(blob);
        resolve(data.data?.url);
      },
      () => {
        if (onError) onError();
      },
    );
  });
}
