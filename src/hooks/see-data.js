import { ref } from 'vue';
import { catchError } from '../helper/async.catch';

export default function useSeeOne(options = { returnAsArray: false }) {
  const seeData = ref([]);
  const seeDialog = ref(false);
  const sizePage = ref(10);
  const memberValue = ref('');

  const openSeeDialog = (apiURL, options = {}, callback = () => {}) => {
    seeDialog.value = true;
    catchError(async () => {
      const result = await apiURL(options);
      if (result.status == 200) {
        seeData.value = result.data?.data?.list;
        callback(result.data?.data);
      } else if (result.status == 204) {
        seeData.value = [];
      }
    });
  };

  const closeSee = () => {
    seeDialog.value = false;
  };

  if (options.returnAsArray) {
    return [seeData, seeDialog, closeSee, openSeeDialog, sizePage, memberValue];
  } else {
    return { seeData, seeDialog, closeSee, openSeeDialog, sizePage, memberValue };
  }
}
