import { ref } from 'vue';
import { isObjectEmpty } from '../helper/utils';
import { catchError } from '../helper/async.catch';

export default function useViewDetail(options = { returnAsArray: false }) {
  const viewData = ref({});
  const viewDialog = ref(false);
  const isLoading = ref(false);

  // const openViewDialog = async (ObjectView) => {
  //   viewDialog.value = true;
  //   viewData.value = ObjectView;
  // };

  const closeView = () => (viewDialog.value = false);

  const viewDataByID = (apiURL, ID, isLoadingInside = false, isForceCallAndSave = false) => {
    const callAndSave = async () => {
      //const { data } = await apiURL(ID);
      //dataRef.value = data.data;
      const result = await apiURL(ID);
      if (result.status == 200 || result.status == 201) {
        viewData.value = result.data?.data;
        viewDialog.value = true;
      } else {
        viewData.value = {};
      }
    };

    if (isObjectEmpty(viewData.value)) {
      if (isLoadingInside) {
        isLoading.value = true;
        catchErrorFinally(
          async () => callAndSave(),
          () => (isLoading.value = false),
        );
      } else {
        catchError(async () => callAndSave());
      }
    }

    if (isForceCallAndSave) {
      callAndSave();
    }
  };

  if (options.returnAsArray) {
    return [viewData, viewDialog, closeView, viewDataByID];
  } else {
    return { viewData, viewDialog, closeView, viewDataByID };
  }
}
