import { ref, computed } from 'vue';
import { useRoute, useRouter } from 'vue-router';
import envConfig from '../helper/env.config';
import { catchError } from '../helper/async.catch';

export default function usePagination() {
  const route = useRoute();
  const router = useRouter();
  const pageSizeEnv = parseInt(envConfig.pageSize);
  const search = ref(null);
  const listData = ref(null);
  const rowTotal = ref(null);
  const pageIndex = computed(() => (route.query.pageIndex ? parseInt(route.query.pageIndex) : 1));
  const pageSize = computed(() => (route.query.pageSize ? parseInt(route.query.pageSize) : pageSizeEnv));

  //SHOW FULL LOADING BEFORE THE DATA IS OBTAINED
  const getListData = (apiURL, objectIndex = pageIndex.value, objectSize = pageSize.value, textSearch = search.value) => {
    search.value = textSearch;
    catchError(async () => {
      const result = await apiURL(objectIndex, objectSize, textSearch);
      if (result.status == 200) {
        listData.value = result.data?.data?.list;
        rowTotal.value = result.data?.data?.TOTAL_ROWS;
      } else if (result.status == 204) {
        listData.value = [];
      }
    });
  };

  // objectIndex = pageIndex.value, objectSize = pageSize.value, filterData = filterData.value
  const getListOptions = (apiURL, objectIndex = pageIndex.value, objectSize = pageSize.value, options = {}) => {
    catchError(async () => {
      const result = await apiURL(objectIndex, objectSize, options);
      if (result.status == 200) {
        listData.value = result.data?.data?.list;
        rowTotal.value = result.data?.data?.TOTAL_ROWS;
      } else if (result.status == 204) {
        listData.value = [];
        rowTotal.value = 0;
      }
    });
  };

  const changeIndex = (funcGetList, newIndex, newPageSize) => {
    router.push(`${route.path}?pageIndex=${newIndex}&pageSize=${newPageSize ? newPageSize : pageSize.value}`);
    funcGetList();
  };
  const changeIndexFilter2 = (funcGetList, newIndex, newPageSize, newFilter) => {
    router.push(`${route.path}?pageIndex=${newIndex}&pageSize=${newPageSize ? newPageSize : pageSize.value}&CLIENT_ID=${newFilter ? newFilter : 0}`);
    funcGetList();
  };

  const changeIndexFilter = (funcGetList, newFilter) => {
    router.push(`${route.path}?CLIENT_ID=${newFilter ? newFilter : 0}`);
    funcGetList();
  };
  const indexFilterStr = (funcGetList, newFilter) => {
    router.push(`${route.path}?CATEGORY_ID=${newFilter ? newFilter : ''}`);
    funcGetList();
  };
  const changeIndexFilterStr2 = (funcGetList, newIndex, newPageSize, newFilter) => {
    router.push(
      `${route.path}?pageIndex=${newIndex}&pageSize=${newPageSize ? newPageSize : pageSize.value}&CATEGORY_ID=${newFilter ? newFilter : ''}`,
    );
    funcGetList();
  };
  const changeNotParams = (funcGetList) => {
    router.push(`${route.path}`);
    funcGetList();
  };

  return [
    listData,
    rowTotal,
    pageIndex,
    pageSize,
    getListData,
    changeIndex,
    getListOptions,
    changeIndexFilter,
    changeNotParams,
    changeIndexFilter2,
    indexFilterStr,
    changeIndexFilterStr2,
  ];
}
