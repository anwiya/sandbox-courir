import { ref } from 'vue';
import { isObjectEmpty } from '../helper/utils';
import { catchError } from '../helper/async.catch';

export default function useGetDataByID() {
  const dataRef = ref({ list: [] });
  const isLoading = ref(false);

  const getDataByID = (apiURL, ID, isLoadingInside = false, isForceCallAndSave = false) => {
    const callAndSave = async () => {
      //const { data } = await apiURL(ID);
      //dataRef.value = data.data;
      const result = await apiURL(ID);
      if (result.status == 200 || result.status == 201) {
        dataRef.value = result.data?.data;
      } else {
        dataRef.value = { list: [] };
      }
    };

    if (isObjectEmpty(dataRef.value)) {
      if (isLoadingInside) {
        isLoading.value = true;
        catchErrorFinally(
          async () => callAndSave(),
          () => (isLoading.value = false),
        );
      } else {
        catchError(async () => callAndSave());
      }
    }

    if (isForceCallAndSave) {
      callAndSave();
    }
  };

  return [dataRef, getDataByID, isLoading];
}
