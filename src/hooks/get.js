import { ref } from 'vue';
import { catchError } from '../helper/async.catch';

export default function useGet() {
  const refData = ref([]);
  const loading = ref(false);

  function get(apiUrl, payload, callback, loadingInside = true) {
    loading.value = true;
    catchError(
      async () => {
        let response;
        if (payload) {
          response = await apiUrl(payload);
        } else {
          response = await apiUrl();
        }
        if (response.status == 200 || response.status == 201) {
          refData.value = response.data.data;
          if (callback) {
            callback(response.data.data);
          }
          loading.value = false;
        }
        if (response.status == 204) {
          refData.value = [];
          loading.value = false;
        }
      },
      () => {
        refData.value = [];
        loading.value = false;
      },
      () => {
        loading.value = false;
      },
      { load: loadingInside },
    );
  }

  return [refData, get, loading];
}
