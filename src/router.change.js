import { watch, computed } from 'vue';
import store from './store/store.index';

export default function watchRoute(route) {
  const currentPath = computed(() => route.fullPath);
  watch(currentPath, async () => {
    runEveryRouteChange(route);
  });
}
//This function will run on every Route Change
export async function runEveryRouteChange(route) {
  setCurrentMenuName(route);
}

function setCurrentMenuName(route) {
  const title = route.meta?.title;
  store.commit('setCurrentMenuName', title || '');
}
