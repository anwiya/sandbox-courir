import { createRouter, createWebHistory } from 'vue-router';
import routeList from './route.list';
import store from '../store/store.index';
import { toRaw } from 'vue';
import { checkToken } from '../helper/app.utils';

const routes = routeList;

const router = createRouter({
  history: createWebHistory(),
  routes,
});

//note: App.vue run first before this function
//this function will run every page change
router.beforeEach(async (to, from, next) => {
  const auth = checkToken();
  if (!auth) {
    if (to.path === '/login') return next();
  }
  if (!auth) return next('/login');

  if (auth) {
    if (to.path == '/login') return next('/dashboard');
  }
  next();
});

export default router;
