const menuList = localStorage.getItem('leftMenu');
const parsedMenu = JSON.parse(menuList);
let indexMenu;
if (parsedMenu) {
  if (parsedMenu[0].MENU_HEADER_HAS_CHILDREN) {
    indexMenu = `/${parsedMenu[0]?.MENU_HEADER_PATH}/${parsedMenu[0]?.childrenMenu[0]?.MENU_DETAIL_PATH}`;
  } else {
    indexMenu = `/${parsedMenu[0]?.MENU_HEADER_PATH}`;
  }
} else {
  indexMenu = '/login';
}

export const routeList = [
  {
    path: '/',
    redirect: indexMenu,
  },
  {
    path: '/:pathMatch(.*)*',
    redirect: '/not-found',
  },
  {
    path: '/login',
    name: 'login',
    component: () => import('../views/login/LoginPage.vue'),
  },
  {
    path: '/dashboard',
    name: 'dashboard',
    meta: {
      title: 'Dashboard',
    },
    component: () => import('../views/dashboard/DashboardPage.vue'),
  },
  {
    path: '/quotation',
    name: 'quotation',
    meta: {
      title: 'Penawaran',
    },
    component: () => import('../views/agent/AgentPage.vue'),
  },
  {
    path: '/order',
    name: 'order',
    meta: {
      title: 'Order',
    },
    component: () => import('../views/order/MakeOrder.vue'),
  },
  {
    path: '/not-found',
    name: 'NotFound',
    component: () => import('../views/NotFound.vue'),
  },
];

export default routeList;
