import { axiosNotAuth, axiosAuth } from '../helper/axios.config';

export async function login(data) {
  return axiosNotAuth.post('/auth/login', data);
}

export async function refresh() {
  return axiosAuth.get('/auth/refresh-token');
}
