import { axiosAuth } from '../helper/axios.config';

export function upload(imageFile) {
  const formData = new FormData();
  formData.append('file', imageFile);
  return axiosAuth.post('/photo/upload', formData, {
    headers: {
      'Content-Type': 'multipart/form-data',
    },
  });
}
