import { axiosAuth } from '../helper/axios.config';

export function cities() {
  return axiosAuth.get('/quotation/cities-with-sdkclient');
}

export function quotation(data) {
  return axiosAuth.post('/quotation/quotation-with-sdkclient', {
    scheduleAt: data.scheduleAt,
    serviceType: data.SERVICE_TYPE,
    specialRequests: data.SPECIAL_REQUEST,
    language: 'id_ID',
    stops: [
      {
        coordinates: {
          lat: data.SENDER_ADDRESS_LAT,
          lng: data.SENDER_ADDRESS_LONG,
        },
        address: data.SENDER_ADDRESS,
      },
      {
        coordinates: {
          lat: data.RECEIVER_ADDRESS_LAT,
          lng: data.RECEIVER_ADDRESS_LONG,
        },
        address: data.RECEIVER_ADDRESS,
      },
    ],
  });
}
export function quotationDetail(data) {
  return axiosAuth.post('/quotation/detail-with-sdkclient', {
    quotationId: data.quotationId,
  });
}
