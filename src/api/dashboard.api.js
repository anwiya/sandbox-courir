import { axiosAuth } from '../helper/axios.config';

export function consult(dateFrom, dateTo) {
  return axiosAuth.post('/dashboard/consultation-summary-v2', {
    DATE_FROM: dateFrom,
    DATE_TO: dateTo,
  });
}

export function doctors(dateFrom, dateTo) {
  return axiosAuth.post('/dashboard/doctor-summary-v2', {
    DATE_FROM: dateFrom,
    DATE_TO: dateTo,
  });
}

export function tipe(dateFrom, dateTo, type) {
  return axiosAuth.post('/dashboard/consultation-summary-detail-v2', {
    DATE_FROM: dateFrom,
    DATE_TO: dateTo,
    TYPE: type,
  });
}

export function waStatus() {
  return axiosAuth.get('/dashboard/wa-provider-status');
}

export function emailStatus() {
  return axiosAuth.get('/dashboard/kirim-email-provider-status');
}
