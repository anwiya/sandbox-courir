import { axiosAuth } from '../helper/axios.config';

export function quotationCity() {
  return axiosAuth.get('/quotation/city');
}

export function requestQuotation(data) {
  return axiosAuth.post('/quotation/quotation-v3', {
    data: {
      scheduleAt: data.scheduleAt,
      serviceType: data.SERVICE_TYPE,
      specialRequests: data.SPECIAL_REQUEST,
      language: 'id_ID',
      stops: [
        {
          coordinates: {
            lat: data.SENDER_ADDRESS_LAT,
            lng: data.SENDER_ADDRESS_LONG,
          },
          address: data.SENDER_ADDRESS,
        },
        {
          coordinates: {
            lat: data.RECEIVER_ADDRESS_LAT,
            lng: data.RECEIVER_ADDRESS_LONG,
          },
          address: data.RECEIVER_ADDRESS,
        },
      ],
    },
  });
}

export function requestOrder(data) {
  return axiosAuth.post('/order/order-v3', {
    data: {
      quotationId: data.QUOTATION_ID,
      sender: {
        stopId: data.SENDER_STOP_ID,
        name: data.SENDER_NAME,
        phone: data.SENDER_PHONE_NO,
      },
      recipients: [
        {
          stopId: data.RECEIVER_STOP_ID,
          name: data.RECEIVER_NAME,
          phone: data.RECEIVER_PHONE_NO,
          remarks: data.RECEIVER_REMARKS,
        },
      ],
      isPODEnabled: data.POD,
      isRecipientSMSEnabled: data.SMS,
      partner: data.PARTNER_NAME,
      metadata: {
        pharmacyOrderId: data.ORDER_ID,
        pharmacyName: data.PHARMACY_NAME,
      },
    },
  });
}

export function orderDetail(data) {
  return axiosAuth.post('/order/order-detail', data);
}

export function orderCancel(data) {
  return axiosAuth.delete(`/order/cancel-order-v3/${data}`);
}

export default {
  requestQuotation,
  requestOrder,
  orderDetail,
  orderCancel,
};
